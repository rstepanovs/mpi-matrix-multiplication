#Parralel matrix multiplication using Fox algorithm

##Description

Python 3 program reads in matrix A and multiplies it with A<sup>T</sup> using Fox algorithm in parralel on N processors specified in run configuration.

After reading in matrix, it pads matrix so that all distributed blocks are equal size and distributes each from process 0 to all processes. It is done with blocking `send` and `receive`, but could be improved with `scatter`.

Matrix is distributed with slice A (i,j) and B (j,i) and B slice is transposed locally
Cartesian communicator `cart_comm` is created to be able to rotate matrix B blocks + creating row communicator `row_comm = cart_comm.Sub([False, True])` to distribute blocks among rows.

To collect result, `gather` is used
 
---

###Limitations

Process count must be perfect square

---


##Input

File Input.txt with matrix dimension  following by space-separated matrix

_Example:_

```
3
1 2 3
0 1 1
0 0 1
```
---
##Output

File Output.txt will be produced and **overwritten** with space separated matrix product


##Running the code


`mpirun -n X python3 Fox.py`
where X is processor count

_Example_
 
`mpirun -n 4 python3 Fox.py`
