from pprint import pprint

import numpy as np
from mpi4py import MPI

comm = MPI.COMM_WORLD


def pad(new_size, matrix):
    result = np.zeros([new_size, new_size])
    result[:matrix.shape[0], :matrix.shape[1]] = matrix
    return result


def main():
    processes = comm.size
    process_rank = comm.rank
    processes_per_row = int(processes ** 0.5)

    submatrix_dim = None
    N = None
    padding = None
    matrix = None

    if process_rank == 0:
        with open('Input.txt', 'r') as f:
            N = int(f.readline())
            matrix = np.asarray([[int(num) for num in line.split(' ')] for line in f])
        padding = (processes - N % processes) % processes
        N = N + padding
        submatrix_dim = int(N / processes_per_row)

    submatrix_dim = comm.bcast(submatrix_dim, root=0)

    cart_comm = comm.Create_cart((processes_per_row, processes_per_row), periods=(True, True), reorder=True)

    local_C = np.zeros([submatrix_dim, submatrix_dim], dtype=int)

    local_A, local_B = propogate_matrix(N, process_rank, processes_per_row, submatrix_dim, matrix)

    above, below = cart_comm.Shift(0, 1)

    row_comm = cart_comm.Sub([False, True])

    for iteration in range(0, processes_per_row):
        row_sender = (process_rank // processes_per_row + iteration) % processes_per_row
        received = row_comm.bcast(local_A, root=(row_sender))
        local_C = local_C + np.matmul(received, local_B)
        local_B = comm.sendrecv(local_B, above)

    gathered = comm.gather(local_C, root=0)
    if process_rank == 0:
        multiplied = np.zeros([N, N], dtype=int)
        for process, values in enumerate(gathered):
            start_row = ((process) // processes_per_row) * submatrix_dim
            start_col = ((process) % processes_per_row) * submatrix_dim
            multiplied[start_row:(start_row + submatrix_dim), start_col:(start_col + submatrix_dim)] = np.asarray(
                values)

        np.savetxt('Output.txt', multiplied[:N - padding, :N - padding], fmt="%d", delimiter="\t")

    MPI.Finalize()


def propogate_matrix(N, process_rank, processes_per_row, submatrix_dim, matrix):
    if process_rank == 0:

        A = pad(N, matrix)
        i = 0
        local_A = None
        local_B = None
        for rowProcess in range(0, processes_per_row):
            for colProcess in range(0, processes_per_row):
                next_slice_A = A[rowProcess * submatrix_dim:((rowProcess + 1) * submatrix_dim),
                               (colProcess * submatrix_dim):(colProcess + 1) * submatrix_dim]
                next_slice_B = A[(colProcess * submatrix_dim):(colProcess + 1) * submatrix_dim,
                               rowProcess * submatrix_dim:((rowProcess + 1) * submatrix_dim)]
                if rowProcess == 0 and colProcess == 0:
                    local_A = next_slice_A
                    local_B = np.transpose(next_slice_B)
                else:
                    comm.send(next_slice_A, i)
                    comm.send(next_slice_B, i)
                i = i + 1
    else:
        local_A = comm.recv()
        local_B = np.transpose(comm.recv())
    comm.Barrier()
    return local_A, local_B


if __name__ == "__main__":
    main()
